Documentation for how to run the codes.
(1) codes - Contains the all the codes
(2) dataset - Contains the dataset

Weighted Case Pipeline
  (a) create_workers_tasks.py - Splits the main dataset into two files namely workers (workers.txt) and tasks (tasks.txt)
  (b) generateRandomRates.py - Generates a random arrival rate for each tasks type. Writes in file (arrival_rates_tasks.txt) 
  (c) getEdges.py - Creates the edges between the tasks and workers. Refer the paper for details on how the edges are created.
                    The worker types are in workers.txt. The task types are in tasks.txt. The edges are stored in edges.txt
  (d) getLP_natural_random.py - Computes the A, b, w matrix for the simple LP. Reads the edge data from edges.txt, 
                                worker data from workers.txt, task data from tasks.txt and arrival rates from 
                                arrival_rates_tasks.txt 
  (e) lp_solver.m - Solves the LP. The X values are stored in X.txt and the LPvalue is stored in LPval.txt

If you use parts of this code, please cite our paper using the following bibtex

@inproceedings{DSSX18,
  title={Assigning Tasks to Workers based on Historical Data: Online Task Assignment with Two-sided Arrivals},
  author={Dickerson, John and Sankararaman, Karthik Abinav and Srinivasan, Aravind and Xu, Pan},
  booktitle={Proceedings of the 17th Conference on Autonomous Agents and MultiAgent Systems},
  year={2018}
}
