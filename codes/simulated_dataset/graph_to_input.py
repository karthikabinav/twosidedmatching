import sys
import random

filename=sys.argv[1]
U=int(sys.argv[2])
c=int(sys.argv[3])

f=open(filename, "r")

f_worker=open(filename + "_worker.txt", "w+")
f_task=open(filename + "_task.txt", "w+")
f_edges=open(filename + "_edge.txt", "w+")

tasks=dict()
total=0
for i in xrange(U):
  r=random.randint(1, 100)
  f_worker.write(str(i) + "," + str(r) + "\n")

for i in xrange(U,c*U+U):
  r = random.random()
  tasks[i]=r
  total+=r

for key in tasks:
  f_task.write(str(key) + "," + str(tasks[key]/float(total)) + "\n")

count=0
for line in f:
  count+=1
  if count<=3:
    continue
  if count>33:
    break
  vals=line.split(" ")
  f_worker.write(vals[0] + "\n")
  
  for i in xrange(1,len(vals)):
    tk=vals[i].split("\n")[0]
    w=random.randint(1,100)
    f_edges.write(vals[0] + ";" + tk + "," + str(w) + "\n")


