from networkx import *
from networkx.algorithms import bipartite
import sys

c=int(sys.argv[1])
p=float(sys.argv[2])
n=int(sys.argv[3])

m=c*n

RB=bipartite.random_graph(n,m, p)
write_adjlist(RB,sys.stdout)

#RB_top = set(n for n,d in RB.nodes(data=True) if d['bipartite']==0)
#RB_bottom = set(RB) - RB_top
#print list(RB_top)
#print list(RB_bottom)

