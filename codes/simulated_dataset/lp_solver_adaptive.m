val = csvread(strcat(filename, '_A_vals_adaptive.txt'));
I = csvread(strcat(filename, '_A_I_adaptive.txt'));
J = csvread(strcat(filename, '_A_J_adaptive.txt'));

b = csvread(strcat(filename, '_b_adaptive.txt'));
c = csvread(strcat(filename, '_weights_adaptive.txt'));


A = sparse(I, J, val);
options = optimoptions(@linprog,'Display', 'iter')
[x, fval] = linprog(-c,A,b,[], [], zeros(size(c)), [], options);

csvwrite(strcat(filename, '_LPval_uniform_adaptive.txt'), fval);
csvwrite(strcat(filename, '_X_uniform_adaptive.txt'), x);
