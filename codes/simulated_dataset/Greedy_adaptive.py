import sys
import random

filename=sys.argv[1]
T=int(sys.argv[2])

EXT=".txt"

f_X=open(filename + "_X_uniform_adaptive" + EXT, "r")
f_val=open(filename + "_LPval_uniform_adaptive" + EXT, "r")

f_workers=open(filename + "_worker_adaptive"  + EXT, "r")
f_edges=open(filename + "_edge_adaptive"  + EXT, "r")
f_rates=open(filename + "_task_adaptive"  + EXT, "r")

edgeWeights=dict()
edgeNumber=dict()
count=0
for line in f_edges:
  vals=line.split(",")
  edgeWeights[vals[0]]=vals[1].split("\n")[0]
  edgeNumber[vals[0]]=count
  count+=1

X=dict()
count=0
for line in f_X:
  X[count]=line.split("\n")[0]
  count+=1

totTypes=0
for line in f_rates:
  totTypes+=1

tasks=list()

arr_rate=dict()
f_rates.seek(0)
for line in f_rates:
  vals=line.split(",")
  tasks.append(vals[0])
  arr_rate[vals[0]]=float(line.split(",")[1].split("\n")[0])

#Simulating the algorithm
f_worker_seq=open(filename + "_worker_sequence.txt", "r").readlines()
f_task_seq=open(filename + "_task_sequence.txt", "r").readlines()


availableWorkers=list()
weight=0
for t in xrange(1,T+1):
  #At time t first sample a worker
  ws=f_worker_seq[t-1]
  ws=int(ws.split("\n")[0])
  if ws!=-1:
    availableWorkers.append(ws)
  ts=f_task_seq[t-1]
  sampledTask=-1
  if ts!=-1:
    sampledTask=ts.split("\n")[0]
  
  #No task was sampled
  if sampledTask==-1:
    continue
  
  chosenEdge=""
  chosenWeight=-1
  
  for w in availableWorkers:
    edge=str(int(w)) + ";" + str(sampledTask) 
    if edge not in edgeNumber:
      continue
    
    if float(edgeWeights[edge]) > chosenWeight:
      chosenEdge=edge
      chosenWeight=float(edgeWeights[edge])
  if chosenWeight==-1:
    continue
  weight+=chosenWeight
  availableWorkers.remove(int(chosenEdge.split(";")[0]))

f_opt=open(filename + "_LPval_uniform_adaptive.txt","r")
opt=0
for line in f_opt:
  opt=float(line.split("\n")[0])*-1

print float(weight)/opt


