import sys
import random
from collections import deque

filename=sys.argv[1]
T=int(sys.argv[2])

ROOT=filename
EXT=".txt"

f_X=open(ROOT + "_X_uniform_adaptive" + EXT, "r")
f_val=open(ROOT + "_LPval_uniform_adaptive" + EXT, "r")

f_workers=open(ROOT + "_worker_adaptive" + EXT, "r")

f_edges=open(ROOT + "_edge_adaptive" + EXT, "r")
f_tasks=open(ROOT + "_task_adaptive" + EXT, "r")

edgeWeights=dict()
revEdgeNumber=dict()
neigh_T=dict()
count=0
for line in f_edges:
  vals=line.split(",")
  edgeWeights[vals[0]]=vals[1].split("\n")[0]
  revEdgeNumber[count]=vals[0]
  tk = vals[0].split(";")[1]
  if tk not in neigh_T:
    neigh_T[tk]=list()
  neigh_T[tk].append(int(vals[0].split(";")[0]))
  count+=1

X=dict()
count=0
for line in f_X:
  X[revEdgeNumber[count]]=line.split("\n")[0]
  count+=1

totTypes=0
for line in f_tasks:
  totTypes+=1

tasks=list()

arr_rate=dict()

f_workers.seek(0)
size_U=0
for line in f_workers:
  size_U+=1

f_tasks.seek(0)
for line in f_tasks:
  vals=line.split(",")
  tasks.append(vals[0])
  arr_rate[vals[0]]=float(line.split(",")[1].split("\n")[0])


#Simulating the algorithm
f_worker_seq=open(filename + "_worker_sequence.txt", "r").readlines()
f_task_seq=open(filename + "_task_sequence.txt", "r").readlines()

availableWorkers=list()
weight=0
for t in xrange(1,T+1):
  #At time t first sample a worker
  ws=f_worker_seq[t-1]
  ws=int(ws.split("\n")[0])
  if ws!=-1:
    availableWorkers.append(ws)
  ts=f_task_seq[t-1]
  sampledTask=-1
  if ts!=-1:
    sampledTask=ts.split("\n")[0]

    
  #No task was sampled
  if sampledTask==-1:
    continue
  
  #Select an edge from LP solution  
  chosenEdge=-1
  Y_val=dict()
  total=0
  if sampledTask not in neigh_T:
    continue
  aset = set(availableWorkers)  
  listCommon = [val for val in neigh_T[sampledTask] if val in aset]

  for w in listCommon:
    edge=str(int(w)) + ";" + str(sampledTask)
    edge_x=float(X[edge])/(float(arr_rate[sampledTask])*T)
    Y_val[edge]=edge_x
    total+=edge_x
  Y_val[str(-1) + ";" + str(-1)]=max(0,1-total)
  S = [(k, Y_val[k]) for k in sorted(Y_val, key=Y_val.get, reverse=False)]
  l1_key = list()
  l1_value=list()

  l2_key_temp = list()
  l2_value_temp=list()
  
  for key in S:
    l1_key.append(key[0])
    l1_value.append(key[1])
    l2_key_temp.append(key[0])
    l2_value_temp.append(key[1])
  
  l2_key=list()
  l2_value=list()
  
  L=len(l2_key_temp)
  for i in xrange(L):
    it = (L + i - 1)%L
    l2_key.append(l2_key_temp[it])
    l2_value.append(l2_value_temp[it])
    
  r=random.random()
  cur1=0
  cur2=0
  firstChoice="-1;-1"
  secondChoice="-1;-1"
  
  for i in xrange(len(l1_value)):
    if cur1+l1_value[i]>=r:
      firstChoice=l1_key[i]
      break
    cur1+=l1_value[i]
  
  for i in xrange(len(l2_value)):
    if cur2+l2_value[i]>=r:
      secondChoice=l2_key[i]
      break
    cur2+=l2_value[i]
  

  firstU = int(firstChoice.split(";")[0])
  secondU = int(secondChoice.split(";")[0])
  if firstU in availableWorkers:
    chosenEdge=firstChoice
  elif secondU in availableWorkers: 
    chosenEdge=secondChoice
  if chosenEdge==-1:
    continue
  
  weight+=float(edgeWeights[chosenEdge])
  availableWorkers.remove(int(chosenEdge.split(";")[0]))

f_opt=open(filename + "_LPval_uniform_adaptive.txt","r")
opt=0
for line in f_opt:
  opt=float(line.split("\n")[0])*-1

print float(weight)/opt


