val = csvread(strcat(filename, '_A_vals.txt'));
I = csvread(strcat(filename, '_A_I.txt'));
J = csvread(strcat(filename, '_A_J.txt'));

b = csvread(strcat(filename, '_b.txt'));
c = csvread(strcat(filename, '_weights.txt'));


A = sparse(I, J, val);
options = optimoptions(@linprog,'Display', 'iter')
[x, fval] = linprog(-c,A,b,[], [], zeros(size(c)), ones(size(c)), [], options);

csvwrite(strcat(filename, '_LPval_uniform.txt'), fval);
csvwrite(strcat(filename, '_X_uniform.txt'), x);
