import sys
import numpy as np
from cvxopt import matrix

filename=sys.argv[1]

EXT=".txt"

f_w=open(filename + "_worker_adaptive"+ EXT, "r")
f_t=open(filename +"_task_adaptive" + EXT, "r")
f_edges=open(filename + "_edge_adaptive" + EXT, "r")

f_weights=open(filename + "_weights_adaptive" + EXT, "w+")
f_A_vals=open(filename + "_A_vals_adaptive"  + EXT, "w+")
f_A_I=open(filename + "_A_I_adaptive"  + EXT, "w+")
f_A_J=open(filename + "_A_J_adaptive"  + EXT, "w+")
f_b=open(filename + "_b_adaptive"  + EXT, "w+")


size_U=0
size_V=0
for line in f_w:
  size_U+=1

for line in f_t:
  size_V+=1
f_w.seek(0)
f_t.seek(0)
f_edges.seek(0)

count=0
rates=dict()
arr_rates=dict()
for line in f_t:
  rates[count]=float(line.split(",")[1].split("\n")[0])*size_U
  arr_rates[line.split(",")[0]]=float(line.split(",")[1].split("\n")[0])*size_U
  count+=1

b=list()
for i in xrange(size_V):
  b.append(rates[i])

for i in xrange(size_U):
  b.append(1)

#Last few columns of b matrix after the following step

neigh_W=dict()
neigh_T=dict()
task_map=dict()
count=1
task_count=1
weights=list()
for line in f_edges:
  vals=line.split(",")
  weights.append(float(vals[1]))
  worker=vals[0].split(";")[0]
  task=vals[0].split(";")[1]
  if task not in task_map:
    task_map[task]=task_count
    task_count+=1
  if worker not in neigh_W:
    neigh_W[worker]=list()
  neigh_W[worker].append(count)
  if task_map[task] not in neigh_T:
    neigh_T[task_map[task]]=list()
  neigh_T[task_map[task]].append(count)
  count+=1

f_edges.seek(0)
num_edges=0
for e in f_edges:
  v = e.split(",")[0].split(";")[1]
  r_v = float(arr_rates[v])
  b.append(0.632*r_v)
  num_edges+=1

vals=list()
I=list()
J=list()
#for r in xrange(1, size_U+size_V+num_edges+1):
#  vals.append(0)
#  I.append(r)
#  J.append(1)

#print num_edges
for key in neigh_T:
  for JJ in neigh_T[key]:
    vals.append(1)
    I.append(int(float(key)))
    J.append(JJ)

for key in neigh_W:
  for JJ in neigh_W[key]:
    vals.append(1)
    I.append(int(float(key)) + size_V + 1)
    J.append(JJ)

for i in xrange(1, num_edges+1):
  vals.append(1)
  I.append(size_U+size_V+i)
  J.append(i)


np.array(vals).tofile(f_A_vals, sep=",")
f_A_vals.close()
np.array(I).tofile(f_A_I, sep=",")
f_A_I.close()
np.array(J).tofile(f_A_J, sep=",")
f_A_J.close()
b=matrix(b)
np.array(b).tofile(f_b, sep=",")
f_b.close()
W=matrix(weights)
np.array(W).tofile(f_weights, sep=",")
f_weights.close()



