import sys
import random

filename=sys.argv[1]
T=int(sys.argv[2])

EXT=".txt"
f_workers=open(filename + "_worker_adaptive"  + EXT, "r")
f_edges=open(filename + "_edge_adaptive"  + EXT, "r")
f_rates=open(filename + "_task_adaptive"  + EXT, "r")

arr_rate=dict()
tasks=list()
f_rates.seek(0)
for line in f_rates:
  vals=line.split(",")
  tasks.append(vals[0])
  arr_rate[vals[0]]=float(line.split(",")[1].split("\n")[0])

f_worker_seq=open(filename + "_worker_sequence.txt", "w+")
f_task_seq=open(filename + "_task_sequence.txt", "w+")

for t in xrange(1,T+1):
  r=random.random()
  cur=0
  ws=-1
  for w in xrange(T):
    if cur + 1/float(T) >= r:
      ws=0
      f_worker_seq.write(str(w) + "\n")
      break
    cur+=1/float(T)
  if ws==-1:
    f_worker_seq.write(str(T-1) + "\n")
  r=random.random()
  cur=0
  sampledTask=-1
  for i in tasks:
    if cur + arr_rate[i] >= r:
      sampledTask=i
      f_task_seq.write(str(sampledTask) + "\n")
      break
    cur+=arr_rate[i]
  if sampledTask==-1:
    f_task_seq.write("-1\n")

