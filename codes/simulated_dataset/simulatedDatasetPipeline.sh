#!/bin/bash

module load matlab/2016b

for p in `seq 0.1 0.1 1.0`;
do
    FILE=../../datasets/simulatedDataset/cconstant_graph_100_$p
    #python generateErdosRenyi.py 100 $p > $FILE
    #python graph_to_input.py $FILE
    #python getLP_natural_random.py $FILE
    #matlab -nodisplay -nosplash -nodesktop -r "filename='$FILE';run('~/two_sided_arrival_matching/codes/simulated_dataset/lp_solver.m');exit;"
    #python NADAP.py $FILE 30
    #python LP_scaled.py $FILE 30
    #python Random.py $FILE 30
    #python Greedy.py $FILE 30
done

for c in `seq 10 10 100`;
do
    #n=100
    FILE=../../datasets/simulatedDataset/pconstant_graph_0.3_$c
    #python generateErdosRenyi.py $c 0.3 $n > $FILE
    #python graph_to_input.py $FILE $n $c
    #python getLP_natural_random.py $FILE
    #matlab -nodisplay -nosplash -nodesktop -r "filename='$FILE';run('~/two_sided_arrival_matching/codes/simulated_dataset/lp_solver.m');exit;"
    #python NADAP.py $FILE $n
    #python LP_scaled.py $FILE $n
    #python Random.py $FILE $n
    #python Greedy.py $FILE $n
done

#Algorithms for adaptive algorithm with strengthened LP
for p in `seq 0.1 0.1 1.0`;
do
    FILE=../../datasets/simulatedDataset/cconstant_graph_100_$p
    #python generateErdosRenyi.py 100 $p > $FILE
    #python graph_to_input_adaptive.py $FILE
    #python getLP_stronger.py $FILE
    #matlab -nodisplay -nosplash -nodesktop -r "filename='$FILE';run('~/two_sided_arrival_matching/codes/simulated_dataset/lp_solver_adaptive.m');exit;"
    #python NADAP.py $FILE 30
    #python LP_scaled.py $FILE 30
    #python Random.py $FILE 30
    #python Greedy.py $FILE 30
    #python ADAP.py $FILE 30
done

n=400
c=6
p=0.6

FILE=../../datasets/simulatedDataset/tabulate_results
python generateErdosRenyi.py $c $p  $n > $FILE
python graph_to_input_adaptive.py $FILE $n $c
python getLP_stronger.py $FILE
matlab -nodisplay -nosplash -nodesktop -r "filename='$FILE';run('~/two_sided_arrival_matching/codes/simulated_dataset/lp_solver_adaptive.m');exit;"

> out_nadap
> out_nadap_2
>out_adap
> out_adap_1.6
> out_lpscaled

for T in `seq 1 1 20`;
do
python generateArrivalSequences.py $FILE $n 
python NADAP_adaptive.py $FILE $n >> out_nadap
python NADAP_2_adaptive.py $FILE $n >> out_nadap_2
python ADAP.py $FILE $n >> out_adap
python ADAP_1.6.py $FILE $n >> out_adap_1.6
python LP_scaled_adaptive.py $FILE $n >> out_lpscaled
done

nadap=0
nadap_2=0
adap=0
adap_1_6=0
lpscaled=0

nadap=$(awk '{x+=$0}END{print x/NR}' out_nadap)
nadap_2=$(awk '{x+=$0}END{print x/NR}' out_nadap_2)
adap=$(awk '{x+=$0}END{print x/NR}' out_adap)
adap_1_6=$(awk '{x+=$0}END{print x/NR}' out_adap_1.6)
lpscaled=$(awk '{x+=$0}END{print x/NR}' out_lpscaled)

echo "$nadap & $nadap_2 & $adap & $adap_1_6 & $lpscaled"

