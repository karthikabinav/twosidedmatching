import sys
import random

filename=sys.argv[1]
T=int(sys.argv[2])

EXT=".txt"

f_X=open(filename + "_X_uniform" + EXT, "r")
f_val=open(filename + "_LPval_uniform" + EXT, "r")

f_workers=open(filename + "_worker"  + EXT, "r")
f_edges=open(filename + "_edge"  + EXT, "r")
f_rates=open(filename + "_task"  + EXT, "r")

edgeWeights=dict()
edgeNumber=dict()
count=0
for line in f_edges:
  vals=line.split(",")
  edgeWeights[vals[0]]=vals[1].split("\n")[0]
  edgeNumber[vals[0]]=count
  count+=1

X=dict()
count=0
for line in f_X:
  X[count]=line.split("\n")[0]
  count+=1

totTypes=0
for line in f_rates:
  totTypes+=1

tasks=list()

arr_rate=dict()
f_rates.seek(0)
for line in f_rates:
  vals=line.split(",")
  tasks.append(vals[0])
  arr_rate[vals[0]]=float(line.split(",")[1].split("\n")[0])

#Simulating the algorithm

availableWorkers=list()
weight=0
for t in xrange(1,T+1):
  #At time t first sample a worker
  r=random.random()
  cur=0
  for w in xrange(T):
    if cur + 1/float(T) >= r:
      availableWorkers.append(int(w))
      break
    cur+=1/float(T)
  r=random.random()
  cur=0
  sampledTask=-1
  for i in tasks:
    if cur + arr_rate[i] >= r:
      sampledTask=i
      break
    cur+=arr_rate[i]
  
  #No task was sampled
  if sampledTask==-1:
    continue
  
  #Select an edge randomly  
  chosenEdge=""
  chosenWeight=-1
  
  for w in availableWorkers:
    edge=str(int(w)) + ";" + str(sampledTask) 
    if edge not in edgeNumber:
      continue
    
    if float(edgeWeights[edge]) >= chosenWeight:
      chosenEdge=edge
      chosenWeight=float(edgeWeights[edge])
  if chosenWeight==-1:
    continue
  weight+=chosenWeight
  availableWorkers.remove(int(chosenEdge.split(";")[0]))

print "Greedy = ", weight


