import sys
import random

filename=sys.argv[1]
U=int(sys.argv[2])
c=int(sys.argv[3])

f=open(filename, "r")

f_worker=open(filename + "_worker_adaptive.txt", "w+")
f_task=open(filename + "_task_adaptive.txt", "w+")
f_edges=open(filename + "_edge_adaptive.txt", "w+")

tasks=dict()
workers=dict()
total=0
for i in xrange(U):
  r=random.randint(1, 100)
  workers[i]=r
  f_worker.write(str(i) + "," + str(r) + "\n")

for i in xrange(U,c*U+U):
  r = random.uniform(0, 0.1)
  tasks[i]=r
  total+=r

tot=0
for key in tasks:
  r=random.uniform(0,1)
  m=1
  f_task.write(str(key) + "," + str(m*tasks[key]/(c*U)) + "\n")

count=0

for line in f:
  count+=1
  if count<=3:
    continue
  if count>U+3:
    break
  vals=line.split(" ")
    
  for i in xrange(1,len(vals)):
    tk=vals[i].split("\n")[0]
    f_edges.write(vals[0] + ";" + str(tk) + "," + str(workers[int(vals[0])])  + "\n")


