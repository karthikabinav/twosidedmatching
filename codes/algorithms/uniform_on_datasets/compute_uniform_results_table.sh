#!/bin/bash

runs=100
avg=0
std=0
T=817


> NADAP_uniform_EverySender
for t in `seq 1 1 $runs`;
do
  python NADAP_uniform.py EverySender cap1 $T >> NADAP_uniform_EverySender 
done
avg=$(awk '{x+=$0}END{print x/NR}' NADAP_uniform_EverySender)
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' NADAP_uniform_EverySender)
echo "NADAP = $avg $std"

> LP_scaled_uniform_EverySender
for t in `seq 1 1 $runs`;
do
  python LP_scaled_uniform.py EverySender cap1 $T >> LP_scaled_uniform_EverySender 
done
avg=$(awk '{x+=$0}END{print x/NR}' LP_scaled_uniform_EverySender)
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' LP_scaled_uniform_EverySender)
echo "LP_scaled = $avg $std"


> Random_uniform_EverySender
for t in `seq 1 1 $runs`;
do
  python Random_uniform.py EverySender cap1 $T >> Random_uniform_EverySender 
done
avg=$(awk '{x+=$0}END{print x/NR}' Random_uniform_EverySender)
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' Random_uniform_EverySender)
echo "Random = $avg $std"

> Greedy_uniform_EverySender
for t in `seq 1 1 $runs`;
do
  python Greedy_uniform.py EverySender cap1 $T >> Greedy_uniform_EverySender 
done
avg=$(awk '{x+=$0}END{print x/NR}' Greedy_uniform_EverySender)
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' Greedy_uniform_EverySender)
echo "Greedy = $avg $std"
 

