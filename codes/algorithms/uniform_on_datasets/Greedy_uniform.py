import sys
import random

dataset=sys.argv[1]
filename=sys.argv[2]
T=int(sys.argv[3])

ROOT_LP="../../../datasets/" + dataset + "/LP/"
ROOT="../../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

f_X=open(ROOT_LP + "X_uniform" + EXT, "r")
f_val=open(ROOT_LP + "LPval_uniform" + EXT, "r")

f_workers=open(ROOT + "workers_" + filename + EXT, "r")
f_tasks=open(ROOT + "tasks_scaled_" + filename + EXT, "r")
f_edges=open(ROOT + "edges_" + filename + EXT, "r")
f_rates=open(ROOT + "rates_tasks_"+ filename + EXT, "r")

edgeWeights=dict()
edgeNumber=dict()
count=0
for line in f_edges:
  vals=line.split(",")
  edgeWeights[vals[0]]=vals[1].split("\n")[0]
  edgeNumber[vals[0]]=count
  count+=1

X=dict()
count=0
for line in f_X:
  X[count]=line.split("\n")[0]
  count+=1

totTypes=0
for line in f_rates:
  totTypes+=1

tasks=list()
tasks_x=dict()
tasks_y=dict()

for line in f_tasks:
  vals=line.split(",")
  tasks.append(vals[0])
  tasks_x[vals[0]]=float(vals[2])
  tasks_y[vals[0]]=float(vals[3])

#Simulating the algorithm

availableWorkers=list()
weight=0
for t in xrange(1,T+1):
  #At time t first sample a worker
  r=random.random()
  cur=0
  for w in xrange(1,T+1):
    if cur + 1/float(T) >= r:
      availableWorkers.append(int(w))
      break
    cur+=1/float(T)
  r=random.random()
  cur=0
  sampledTask=-1
  for i in tasks:
    if cur + 1/float(len(tasks)) >= r:
      sampledTask=i
      break
    cur+=1/float(len(tasks))
  
  #No task was sampled
  if sampledTask==-1:
    continue
  
  #Select an edge randomly  
  chosenEdge=""
  chosenWeight=-1
  
  for w in availableWorkers:
    edge=str(int(w)) + ";" + str(round(tasks_x[sampledTask],2)) + ";" +str(round(tasks_y[sampledTask],2)) 
    if edge not in edgeNumber:
      continue
    
    if float(edgeWeights[edge]) >= chosenWeight:
      chosenEdge=edge
      chosenWeight=float(edgeWeights[edge])
  if chosenWeight==-1:
    continue
  weight+=chosenWeight
  availableWorkers.remove(int(chosenEdge.split(";")[0]))

print weight


