#!/bin/bash

runs=10
avg=0
std=0

dataset=gMission
#T=817
T=532

Eta=$1
Lambda=$(($Eta*3))

> NADAP_"$dataset"
for t in `seq 1 1 $runs`;
do
  python NADAP.py $dataset $T $Eta $Lambda>> NADAP_"$dataset"
done
avg=$(awk '{x+=$0}END{print x/NR}' NADAP_"$dataset")
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' NADAP_"$dataset")
echo "NADAP = $avg $std"

> LP_scaled_"$dataset"
for t in `seq 1 1 $runs`;
do
  python LP_scaled.py $dataset $T $Eta $Lambda >> LP_scaled_"$dataset"
done
avg=$(awk '{x+=$0}END{print x/NR}' LP_scaled_"$dataset")
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' LP_scaled_"$dataset")
echo "LP_scaled = $avg $std"


> Random_"$dataset"
for t in `seq 1 1 $runs`;
do
  python Random.py $dataset $T $Eta $Lambda >> Random_"$dataset"
done
avg=$(awk '{x+=$0}END{print x/NR}' Random_"$dataset")
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' Random_"$dataset")
echo "Uniform = $avg $std"

> Greedy_"$dataset"
for t in `seq 1 1 $runs`;
do
  python Greedy.py $dataset $T $Eta $Lambda >> Greedy_"$dataset"
done
avg=$(awk '{x+=$0}END{print x/NR}' Greedy_"$dataset")
std=$(awk '{x+=$0;y+=$0^2}END{print sqrt(y/NR-(x/NR)^2)i}' Greedy_"$dataset")
echo "Greedy = $avg $std"
 

