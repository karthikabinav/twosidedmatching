import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import sys

NADAP=list()
ADAP=list()
Greedy=list()

X=list()

ROOT="./Exp5/"
cnt=0
f=open(ROOT + "waiting_output_NADAP_gMission", "r")
for line in f:
  val=line.split("\n")[0]
  NADAP.append(float(val))
  X.append(cnt)
  cnt+=1

f=open(ROOT + "output_waiting_ADAP_gMission", "r")
for line in f:
  val=line.split("\n")[0]
  ADAP.append(float(val))

f=open(ROOT + "output_waiting_Greedy_gMission", "r")
for line in f:
  val=line.split("\n")[0]
  Greedy.append(float(val))


plt.hold(True)
plt.plot(X, NADAP, label='NADAP', linestyle='-.', color='brown', linewidth=5)
plt.plot(X, ADAP, label='ADAP', linestyle='-', color='red', linewidth=5)
plt.plot(X, Greedy, label='Greedy', linestyle='-', color='blue', linewidth=3)

axis_font={'size' : 30}

plt.title("Average wait times of various workers", **axis_font)
plt.xlabel('Worker ID', **axis_font)
plt.ylabel('Number of Time Steps', **axis_font)
font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 26}

matplotlib.rc('font', **font)
plt.legend(loc='upper left')
#plt.ylim([4.5, 5.5])
plt.show()




