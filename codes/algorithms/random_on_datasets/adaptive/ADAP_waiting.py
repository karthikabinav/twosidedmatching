import sys
import random

dataset=sys.argv[1]
T=int(sys.argv[2])
Eta=sys.argv[3]
Lambda=sys.argv[4]
Delta=T


ROOT_LP="../../../../datasets/" + dataset + "/LP/"
ROOT="../../../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

f_X=open(ROOT_LP + Eta + "_" + Lambda + "_" + "X_adaptive" + EXT, "r")
f_val=open(ROOT_LP + Eta + "_" + Lambda + "_" + "LPval_adaptive" + EXT, "r")

f_workers=open(ROOT + "workers" + EXT, "r")
f_edges=open(ROOT + "edges_adaptive" + EXT, "r")
f_rates=open(ROOT + "arrival_rates_tasks" + EXT, "r")

edgeWeights=dict()
edgeNumber=dict()
count=0
for line in f_edges:
  vals=line.split(",")
  edgeWeights[vals[0]]=vals[1].split("\n")[0]
  edgeNumber[vals[0]]=count
  count+=1

X=dict()
count=0
for line in f_X:
  X[count]=line.split("\n")[0]
  count+=1

totTypes=0
for line in f_rates:
  totTypes+=1

tasks=list()
tasks_x=dict()
tasks_y=dict()

f_arr_rates=open(ROOT + "arrival_rates_tasks" + EXT, "r")
arr_rate=dict()
count=0
for line in f_arr_rates:
  arr_rate[count]=float(line.split("\n")[0])
  count+=1


f_tasks=open(ROOT + "tasks" + EXT, "r")
task_map=dict()
count=0
for line in f_tasks:
  vals=line.split(",")
  tasks.append(vals[0])
  task_map[vals[0]]=count
  count+=1
  tasks_x[vals[0]]=float(vals[1])
  tasks_y[vals[0]]=float(vals[2])

#Data Structure to store the wait times
waitTimes=dict()
numTimes=dict()
for w in xrange(1, T+1):
  waitTimes[int(w)]=1
  numTimes[int(w)]=1

#Simulating the algorithm

availableWorkers=list()
weight=0
for t in xrange(1,T+1):
  #At time t first sample Eta workers
  for eta in xrange(int(Eta)):
    r=random.random()
    cur=0
    for w in xrange(1,T+1):
      if cur + 1/float(T) >= r and cur<r:
        availableWorkers.append((t,int(w)))
        break
      cur+=1/float(T)
  #sample Lambda tasks
  r=random.random()
  cur=0
  sampledTasks=list()
  atLeastOnce=False
  for j in xrange(int(Lambda)):
    cur=0
    for i in tasks:
      if cur + arr_rate[task_map[i]] >= r and cur < r:
        sampledTasks.append(i)
        atLeastOnce=True
        break
      cur+=arr_rate[task_map[i]]
  
  #No task was sampled
  if not atLeastOnce:
    continue

  #Filter the unavailable values
  for workers in availableWorkers:
    if workers[0]<t-int(Delta):
      availableWorkers.remove(workers)
  
  #Select an edge from LP solution  
  for i in xrange(len(sampledTasks)):
    sampledTask=sampledTasks[i]
    chosenEdge=-1
    Y_val=dict()
    edgeWorkerMap=dict()

    total=0
    for w in availableWorkers:
      time=w[0]
      worker=w[1]

      edge=str(float(worker)) + ";" + str(float(sampledTask))

      if edge not in edgeNumber:
        continue
      
      edge_x=float(X[edgeNumber[edge]])/float(arr_rate[task_map[sampledTask]]*T*float(Lambda))

      Y_val[edge]=edge_x
      if edge not in edgeWorkerMap:
        edgeWorkerMap[edge]=list()
      edgeWorkerMap[edge].append(w)
      total+=edge_x
    Y_val[str(-1.0) + ";" + str(-1.0)]=max(0,1-total)
    S = [(k, Y_val[k]) for k in sorted(Y_val, key=Y_val.get, reverse=False)]
    l1_key = list()
    l1_value=list()

    l2_key_temp = list()
    l2_value_temp=list()
    
    for key in S:
      l1_key.append(key[0])
      l1_value.append(key[1])
      l2_key_temp.append(key[0])
      l2_value_temp.append(key[1])
    
    l2_key=list()
    l2_value=list()
    
    L=len(l2_key_temp)
    for i in xrange(L):
      it = (L + i - 1)%L
      l2_key.append(l2_key_temp[it])
      l2_value.append(l2_value_temp[it])
      
    r=random.random()
    cur1=0
    cur2=0
    firstChoice="-1.0;-1.0"
    secondChoice="-1.0;-1.0"
    
    for i in xrange(len(l1_value)):
      if cur1+l1_value[i]>=r and cur<r:
        firstChoice=l1_key[i]
        break
      cur1+=l1_value[i]
    
    for i in xrange(len(l2_value)):
      if cur2+l2_value[i]>=r and cur2<r:
        secondChoice=l2_key[i]
        break
      cur2+=l2_value[i]
    
    
    firstU = float(firstChoice.split(";")[0])
    secondU = float(secondChoice.split(";")[0])
    if firstU!=float(-1.0):
      chosenEdge=firstChoice
    elif secondU!=float(-1.0): 
      chosenEdge=secondChoice
    if chosenEdge==-1:
      continue
    
    weight+=float(edgeWeights[chosenEdge])
    chosenWorker=edgeWorkerMap[chosenEdge][0]
    availableWorkers.remove(chosenWorker)
    
    time=chosenWorker[0]
    worker=chosenWorker[1]
    waitTimes[worker]+=(numTimes[worker]*waitTimes[worker] + (t-time))/(numTimes[worker]+1)
    numTimes[worker]+=1

for w in waitTimes:
  print waitTimes[w]

