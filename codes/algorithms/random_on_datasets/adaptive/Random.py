import sys
import random

dataset=sys.argv[1]
T=int(sys.argv[2])
Eta=sys.argv[3]
Lambda=sys.argv[4]
Delta=T
ROOT_LP="../../../../datasets/" + dataset + "/LP/"
ROOT="../../../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

f_X=open(ROOT_LP + Eta + "_" + Lambda + "_" + "X_adaptive" + EXT, "r")
f_val=open(ROOT_LP + Eta + "_" + Lambda + "_" + "LPval_adaptive" + EXT, "r")

f_workers=open(ROOT + "workers" + EXT, "r")
f_edges=open(ROOT + "edges_adaptive" + EXT, "r")
f_rates=open(ROOT + "arrival_rates_tasks" + EXT, "r")

edgeWeights=dict()
edgeNumber=dict()
count=0
for line in f_edges:
  vals=line.split(",")
  edgeWeights[vals[0]]=vals[1].split("\n")[0]
  edgeNumber[vals[0]]=count
  count+=1

X=dict()
count=0
for line in f_X:
  X[count]=line.split("\n")[0]
  count+=1

totTypes=0
for line in f_rates:
  totTypes+=1

tasks=list()
tasks_x=dict()
tasks_y=dict()

f_arr_rates=open(ROOT + "arrival_rates_tasks" + EXT, "r")
arr_rate=dict()
count=0
for line in f_arr_rates:
  arr_rate[count]=float(line.split("\n")[0])
  count+=1

f_tasks=open(ROOT + "tasks" + EXT, "r")
task_map=dict()
count=0
for line in f_tasks:
  vals=line.split(",")
  tasks.append(vals[0])
  task_map[vals[0]]=count
  count+=1
  tasks_x[vals[0]]=float(vals[1])
  tasks_y[vals[0]]=float(vals[2])


 #Simulating the algorithm
availableWorkers=list()
weight=0
for t in xrange(1,T+1):
  #At time t first sample Eta workers
  for eta in xrange(int(Eta)):
    r=random.random()
    cur=0
    for w in xrange(1,T+1):
      if cur + 1/float(T) >= r and cur<r:
        availableWorkers.append((t,int(w)))
        break
      cur+=1/float(T)

  #sample Lambda tasks
  r=random.random()
  cur=0
  sampledTasks=list()
  atLeastOnce=False
  for j in xrange(int(Lambda)):
    cur=0
    for i in tasks:
      if cur + arr_rate[task_map[i]] >= r and cur < r:
        sampledTasks.append(i)
        atLeastOnce=True
        break
      cur+=arr_rate[task_map[i]]
  
  #No task was sampled
  if not atLeastOnce:
    continue
  
  #Filter the unavailable values
  for workers in availableWorkers:
    if workers[0]<t-int(Delta):
      availableWorkers.remove(workers)
 
  #Select an edge randomly  
  for i in xrange(len(sampledTasks)):
    sampledTask=sampledTasks[i]
    cur=0
    chosenEdge=-1
    chosenWorker=-1
    count=0
    for w in availableWorkers:
      time=w[0]
      worker=w[1]
      edge=str(float(worker)) + ";" + str(float(sampledTask))
      if edge not in edgeNumber:
        continue
      count+=1
    if count == 0:
      continue
    
    r=random.randint(1, count)
    for w in availableWorkers:
      time=w[0]
      worker=w[1]
      edge=str(float(worker)) + ";" + str(float(sampledTask))
      
      if edge not in edgeNumber:
        continue
      
      if cur+1 >= r:
        chosenEdge=edge
        chosenWorker=w
        break

      cur+=1
      
    if chosenEdge==-1:
      continue

    weight+=float(edgeWeights[chosenEdge])
    availableWorkers.remove(chosenWorker)
OPT=0
for line in f_val:
  OPT=-1*float(line.split("\n")[0])

print weight/OPT


