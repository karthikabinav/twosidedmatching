import sys
import numpy as np
from cvxopt import matrix

dataset=sys.argv[1]
filename=sys.argv[2]

ROOTIN="../../datasets/" + dataset + "/afterProcessing/"
ROOTOUT="../../datasets/" + dataset + "/LP/"
EXT=".txt"

f_w=open(ROOTIN + "workers_" + filename + EXT, "r")
f_t=open(ROOTIN + "rates_tasks_" + filename + EXT, "r")
f_edges=open(ROOTIN + "edges_" + filename + EXT, "r")

f_weights=open(ROOTOUT + "weights_" + filename + EXT, "w+")
f_A_vals=open(ROOTOUT + "A_vals_" + filename + EXT, "w+")
f_A_I=open(ROOTOUT + "A_I_" + filename + EXT, "w+")
f_A_J=open(ROOTOUT + "A_J_" + filename + EXT, "w+")
f_b=open(ROOTOUT + "b_uniform_" + filename + EXT, "w+")


size_U=0
size_V=0
for line in f_w:
  size_U+=1

for line in f_t:
  size_V+=1

f_w=open(ROOTIN + "workers_" + filename + EXT, "r")
f_t=open(ROOTIN + "rates_tasks_" + filename + EXT, "r")

b=list()
for i in xrange(size_V):
  b.append(1)

for i in xrange(size_U):
  b.append(1)

neigh_W=dict()
neigh_T=dict()
task_map=dict()
count=1
task_count=1
weights=list()
for line in f_edges:
  vals=line.split(",")
  weights.append(float(vals[1]))
  worker=vals[0].split(";")[0]
  task=vals[0].split(";")[1]+";" +vals[0].split(";")[2]
  if task not in task_map:
    task_map[task]=task_count
    task_count+=1
  if worker not in neigh_W:
    neigh_W[worker]=list()
  neigh_W[worker].append(count)
  if task_map[task] not in neigh_T:
    neigh_T[task_map[task]]=list()
  neigh_T[task_map[task]].append(count)
  count+=1

f_edges.close()

vals=list()
I=list()
J=list()
for key in neigh_T:
  for JJ in neigh_T[key]:
    vals.append(1)
    I.append(int(float(key)))
    J.append(JJ)

for key in neigh_W:
  for JJ in neigh_W[key]:
    vals.append(1)
    I.append(int(float(key) + size_V))
    J.append(JJ)

np.array(vals).tofile(f_A_vals, sep=",")
f_A_vals.close()
np.array(I).tofile(f_A_I, sep=",")
f_A_I.close()
np.array(J).tofile(f_A_J, sep=",")
f_A_J.close()
b=matrix(b)
np.array(b).tofile(f_b, sep=",")
f_b.close()
W=matrix(weights)
np.array(W).tofile(f_weights, sep=",")
f_weights.close()



