root =strcat(strcat('../../datasets/', dataset), '/LP/')

val = csvread(strcat(root, 'A_vals_cap1.txt'));
I = csvread(strcat(root, 'A_I_cap1.txt'));
J = csvread(strcat(root, 'A_J_cap1.txt'));

b = csvread(strcat(root, 'b__uniform_cap1.txt'));
c = csvread(strcat(root, 'weights_cap1.txt'));


A = sparse(I, J, val);
size(A)
size(c)
options = optimoptions(@linprog,'Display', 'iter')
[x, fval] = linprog(-c,A,b,[], [], zeros(size(c)), ones(size(c)), [], options);

csvwrite(strcat(root, 'LPval_uniform.txt'), fval);
csvwrite(strcat(root, 'X_uniform.txt'), x);
