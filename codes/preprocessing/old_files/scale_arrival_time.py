import sys

dataset=sys.argv[1]
filename=sys.argv[2]
arrv_max=int(sys.argv[3])

ROOT="../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

f_tasks=open(ROOT + "tasks_" + filename + EXT, "r") 
f_workers=open(ROOT + "workers_" + filename + EXT, "r")

f_scaled_tasks=open(ROOT + "tasks_scaled_" + filename + EXT, "w+" )

T=0
for line in f_workers:
  T+=1

for line in f_tasks:
  vals=line.split(",")
  arrv_time=int(vals[1])  
  scaled=int(round((arrv_time*T/float(arrv_max))))
  f_scaled_tasks.write(vals[0] + "," + str(scaled) + "," + vals[2] + "," + vals[3] + "," + vals[4] + "," + vals[5])

f_scaled_tasks.close()
   
