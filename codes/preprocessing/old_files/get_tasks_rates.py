import sys

dataset=sys.argv[1]
filename=sys.argv[2]

ROOT="../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

tasks_filename=ROOT + "tasks_" + filename + EXT

f_task=open(tasks_filename, "r")

types=dict()
total=dict()
for line in f_task:
  vals=line.split(",")
  x=float(vals[2])
  y=float(vals[3])
  
  int_x=round(x,2)
  int_y=round(y,2)

  v_type=str(int_x) + ";" + str(int_y)

  if v_type not in types:
    types[v_type]=float(vals[5].split("\n")[0])
    total[v_type]=1
  else:
    types[v_type]+=float(vals[5].split("\n")[0])
    total[v_type]+=1

f_output=open(ROOT + "rates_tasks_" + filename + EXT, "w+")

for key in types:
  f_output.write(key + "," + str(float(types[key])/float(total[key])) + "\n")

f_output.close()
