#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='1';Lambda='3';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='5';Lambda='15';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='10';Lambda='30';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='15';Lambda='45';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='20';Lambda='60';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='25';Lambda='75';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='30';Lambda='90';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='35';Lambda='105';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='40';Lambda='120';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='45';Lambda='135';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"

#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='1';Lambda='5';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='2';Lambda='10';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='3';Lambda='15';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='4';Lambda='20';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='5';Lambda='25';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='6';Lambda='30';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='7';Lambda='35';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"


#matlab -nodisplay -nosplash -nodesktop -r "dataset='gMission';Eta='1';Lambda='1';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
#matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySender';Eta='1';Lambda='1';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"

matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='1';Lambda='5';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='5';Lambda='25';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='10';Lambda='50';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='15';Lambda='75';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='20';Lambda='100';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='25';Lambda='125';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='30';Lambda='150';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"
matlab -nodisplay -nosplash -nodesktop -r "dataset='EverySenderSample';Eta='35';Lambda='175';run('~/two_sided_arrival_matching/codes/preprocessing/lp_solver.m');exit;"





