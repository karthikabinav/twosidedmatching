#!/bin/bash

#Preprocessing to get workers and tasks seperately
#python create_workers_tasks.py gMission
#python create_workers_tasks.py EverySender

#Generate random arrival rates for the tasks
#python generateRandomRates.py gMission
#python generateRandomRates.py EverySender

#Get the edge weights from range, successProb and payoff
#python getEdges.py gMission
#echo "Done"
#python getEdges.py EverySender
#echo "Done"

#Get the LP relaxation (note LP depends only on Lambda)
declare -a Eta=(1 5 10 15 20 25 30 35)
#declare -a Eta=(1 2 3 4 5 6 7)
  for e in "${Eta[@]}";
  do
    #python getLP_adaptive.py gMission $(($e*3)) $e
    #python getLP_adaptive.py EverySender $(($e*5)) $e
    #python getLP_simple.py gMission $(($e*3)) $e
    #python getLP_simple.py EverySender $(($e*5)) $e
    python getLP_simple.py EverySenderSample $(($e*5)) $e
done

#declare -a Lambda=(1 5)
#for l in "${Lambda[@]}";
#do
#  python getLP_simple.py EverySender $l
#done
