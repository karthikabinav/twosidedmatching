root =strcat(strcat(strcat(strcat(strcat(strcat('../../datasets/', dataset), '/LP/'),Eta),'_'), Lambda), '_')

val = csvread(strcat(root, 'A_vals.txt'));
I = csvread(strcat(root, 'A_I.txt'));
J = csvread(strcat(root, 'A_J.txt'));

b = csvread(strcat(root, 'b.txt'));
c = csvread(strcat(root, 'weights.txt'));


A = sparse(I, J, val);
size(A)
size(c)
options = optimoptions(@linprog,'Display', 'iter')
[x, fval] = linprog(-c,A,b,[], [], zeros(size(c)), ones(size(c)), [], options);

csvwrite(strcat(root, 'LPval.txt'), fval);
csvwrite(strcat(root, 'X.txt'), x);
