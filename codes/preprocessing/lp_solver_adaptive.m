root =strcat(strcat(strcat(strcat(strcat(strcat('../../datasets/', dataset), '/LP/'),Eta),'_'), Lambda), '_')

val = csvread(strcat(root, 'A_vals_adaptive.txt'));
I = csvread(strcat(root, 'A_I_adaptive.txt'));
J = csvread(strcat(root, 'A_J_adaptive.txt'));

b = csvread(strcat(root, 'b_adaptive.txt'));
c = csvread(strcat(root, 'weights_adaptive.txt'));

A = sparse(I, J, val);

options = optimoptions(@linprog,'Display', 'iter')
[x, fval] = linprog(-c,A,b,[], [], zeros(size(c)), ones(size(c)), [], options);

csvwrite(strcat(root, 'LPval_adaptive.txt'), fval);
csvwrite(strcat(root, 'X_adaptive.txt'), x);
