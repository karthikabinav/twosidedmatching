#!/bin/bash

# Lines that begin with #SBATCH specify commands to be used by SLURM for scheduling

#SBATCH --job-name=lpad                                   # sets the job name
#SBATCH --output lpad.out.%j                              # indicates a file to redirect STDOUT to; %j is the jobid 
#SBATCH --error lpad.out.%j                               # indicates a file to redirect STDERR to; %j is the jobid
#SBATCH --time=07:59:59                                         # how long you think your job will take to complete; format=hh:mm:ss
#SBATCH --partition dpart
#SBATCH --gres=gpu:0
#SBATCH --nodes=1                                               # number of nodes to allocate for your job
#SBATCH --ntasks=2                                              # request 4 cpu cores be reserved for your node total
#SBATCH --ntasks-per-node=2                                     # request 2 cpu cores be reserved per node
#SBATCH --mem=50gb                                               # memory required by job; if unit is not specified MB will be assumed

source ../../project_files/bin/activate
module load matlab/2016b

srun bash run_code.sh &    # use srun to invoke commands within your job; using an '&'
wait
# once the end of the batch script is reached your job allocation will be revoked
