import sys
import pandas as pd
import math

dataset=sys.argv[1]

ROOT="../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

worker_filename=ROOT + "workers" +  EXT
tasks_filename=ROOT + "tasks"  + EXT

f_edges=open(ROOT + "edges" + EXT, "w+")

df_w = pd.read_csv(worker_filename, sep=',', header=None)
df_t = pd.read_csv(tasks_filename, sep=',', header=None)
df_w.columns=["w_id", "w_x", "w_y", "w_range", "w_successProb"]
df_t.columns=["t_id", "t_x", "t_y", "t_payoff"]

df_w['key'] = 1
df_t['key'] = 1

product = pd.merge(df_w, df_t, on='key')

for index, row in product.iterrows():
  w_x=float(row["w_x"])
  w_y=float(row["w_y"])
  t_x=float(row["t_x"])
  t_y=float(row["t_y"])
  
  dist=math.sqrt((w_x-t_x)**2 + (w_y-t_y)**2)
  if dist <= float(row["w_range"]):
    succ = float(row["w_successProb"])
    payoff = float(row["t_payoff"])
    weight=succ*payoff
    f_edges.write(str(row["w_id"]) + ";" + str(row["t_id"]) + "," + str(weight) + "\n")

f_edges.close()
