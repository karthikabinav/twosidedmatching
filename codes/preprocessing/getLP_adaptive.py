import sys
import numpy as np
from cvxopt import matrix

dataset=sys.argv[1]
Lambda=sys.argv[2]
Eta=sys.argv[3]

ROOTIN="../../datasets/" + dataset + "/afterProcessing/"
ROOTOUT="../../datasets/" + dataset + "/LP/"
EXT=".txt"

f_w=open(ROOTIN + "workers" + EXT, "r")
f_t=open(ROOTIN + "tasks"  + EXT, "r")
f_edges=open(ROOTIN + "edges_adaptive"  + EXT, "r")

f_weights=open(ROOTOUT + Eta + "_" + Lambda + "_" + "weights_adaptive" + EXT, "w+")
f_A_vals=open(ROOTOUT + Eta + "_" + Lambda + "_" + "A_vals_adaptive" + EXT, "w+")
f_A_I=open(ROOTOUT + Eta + "_" + Lambda + "_" + "A_I_adaptive" + EXT, "w+")
f_A_J=open(ROOTOUT + Eta + "_" + Lambda + "_" + "A_J_adaptive" + EXT, "w+")
f_b=open(ROOTOUT + Eta + "_" + Lambda + "_" + "b_adaptive"  + EXT, "w+")


size_U=0
size_V=0
for line in f_w:
  size_U+=1

for line in f_t:
  size_V+=1

f_w=open(ROOTIN + "workers" + EXT, "r")
f_t=open(ROOTIN + "tasks" + EXT, "r")
f_rates=open(ROOTIN + "arrival_rates_tasks" + EXT, "r")

count=0
rates=dict()
for line in f_rates:
  rates[count]=float(line.split("\n")[0])*size_U*float(Lambda)
  count+=1

taskRates=dict()
cnt=0
for line in f_t:
  vals=line.split(",")
  task=str(float(vals[0]))
  taskRates[task]=rates[cnt]
  cnt+=1

neigh_W=dict()
neigh_T=dict()
edgeNumber=dict()
revEdgeNumber=dict()
count=1
weights=list()
for line in f_edges:
  vals=line.split(",")
  weights.append(float(vals[1]))
  worker=vals[0].split(";")[0]
  task=vals[0].split(";")[1]
  edgeNumber[vals[0]]=count
  revEdgeNumber[count]=vals[0]

  if worker not in neigh_W:
    neigh_W[worker]=list()
  neigh_W[worker].append(vals[0])
  if task not in neigh_T:
    neigh_T[task]=list()
  neigh_T[task].append(vals[0])
  count+=1

f_edges.close()

vals=list()
I=list()
J=list()
b=list()
row=0
for key in neigh_T:
  row+=1
  b.append(taskRates[key])
  for JJ in neigh_T[key]:
    vals.append(1)
    I.append(row)
    J.append(edgeNumber[JJ])

for key in neigh_W:
  row+=1
  b.append(float(Eta))
  for JJ in neigh_W[key]:
    vals.append(1)
    I.append(row)
    J.append(edgeNumber[JJ])

for i in xrange(1, count):
  row+=1
  edge=revEdgeNumber[i]
  task=edge.split(";")[1]
  b.append(0.632*taskRates[task])
  vals.append(1)
  I.append(row)
  J.append(i)

print len(I), len(J), len(vals)


np.array(vals).tofile(f_A_vals, sep=",")
f_A_vals.close()
np.array(I).tofile(f_A_I, sep=",")
f_A_I.close()
np.array(J).tofile(f_A_J, sep=",")
f_A_J.close()
b=matrix(b)
np.array(b).tofile(f_b, sep=",")
f_b.close()
W=matrix(weights)
np.array(W).tofile(f_weights, sep=",")
f_weights.close()



