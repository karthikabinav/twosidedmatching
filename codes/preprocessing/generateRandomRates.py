import sys
import random

dataset=sys.argv[1]

ROOT="../../datasets/" + dataset + "/afterProcessing/"
EXT=".txt"

f_t=open(ROOT + "tasks" +EXT, "r")
f_rates=open(ROOT + "arrival_rates_tasks" + EXT, "w+")

arr_rate=dict()
total=0
for line in f_t:
  vals=line.split(",")
  r=random.random()
  arr_rate[vals[0]]=r
  total+=r

for key in arr_rate:
  arr_rate[key]=arr_rate[key]/float(total)
  f_rates.write(str(arr_rate[key]) + "\n")
f_rates.close()

