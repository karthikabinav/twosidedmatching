import sys

dataset=sys.argv[1]

ROOT="../../datasets/" + dataset + "/"
OUT="afterProcessing/"
EXT=".txt"

f=open(ROOT + dataset + "_cap1" + EXT, 'r')
g_w=open(ROOT + OUT + "workers" + EXT, "w+")
g_t=open(ROOT + OUT + "tasks" + EXT, "w+")

first=True
task_num=1
worker_num=1

task_dict=dict()
worker_dict=dict()

for line in f:
  if first:
    first=False
    continue

  vals=line.split(" ")
  if vals[1]=="t" and task_num<1000:
    t_x=round(float(vals[2]),2)
    t_y=round(float(vals[3]),2)
    
    t_type=str(t_x) + ";" + str(t_y)
    if t_type not in task_dict:
      task_dict[t_type]=1
      t_payoff=vals[5]
      g_t.write(str(task_num) + "," + str(t_x) + "," + str(t_y) + "," + t_payoff)
      task_num+=1
  elif vals[1] == "w" and worker_num<205:
    w_x=round(float(vals[2]),2)
    w_y=round(float(vals[3]),2)
    
    w_type=str(w_x) + ";" + str(w_y)
    if w_type not in worker_dict:
      worker_dict[w_type]=1
      w_range=vals[4]
      w_success=vals[7]
      g_w.write(str(worker_num) + "," + str(w_x) + "," + str(w_y) + "," + w_range   + "," + w_success)
      worker_num+=1
g_t.close()
g_w.close()
